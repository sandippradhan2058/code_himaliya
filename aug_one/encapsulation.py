##public access modifier 
#normal

# person ( name , _age,__sn)
# 3 method which access them 

########### Protected 





from unicodedata import name


class emp:
    
    def __init__(self,name='sandip',age=22,sn=1231):
        self.name=name
        self._age = age
        self.__sn =sn
    def public_name(self):
        print('name')
        return self.name
    def _protected_age(self):
        print('\n age ',)
        return self._age
    def __private(self):
        print('\n sn')
        return self.__sn
    def priv_cling_from_public(self):
        print(self.__private())
    def proc_cling_from_public(self):
        print(self._protected_age())
class another_class(emp):
    def __init__(self):
        # super(another_class,self).__init__()
        pass
    
# s= emp('sandip',22,12)
# print(s.public_name())
# print(s._protected_age())

# print(s._emp__sn)
# print(s._emp__private())
c = another_class()
print(another_class.mro())
# print(c.public_name())

# print(c._protected_age())
print(c.public_name())
