# from unicodedata import name


# class person:
#     def __init__(self,name):
#         self.name = name
#         self.is_emp = False

#     def  get_description(self):
#         print('this is a person class desciption ', self.name,self.is_emp)
#         return self.name,self.is_emp

# #single inheritance
# class employee(person):
#     def employee_status(self):
#         self.is_emp = True

# persons = person('abc')
# print('it use parent data',persons.name,persons.is_emp)
# print('it use parent get_description', persons.get_description())

# emplo = employee('sandip')
# emplo.employee_status()
# print('it use employee name and child class employee ',emplo.name,emplo.is_emp)
# print(employee.mro())

    
# class work

# class vehicle :
#     def __init__(self,name):
#         self.name = name
#         self.stock = False
        
    
# class car(vehicle):
#     def vehicle_stock(self):
#         self.stock = True
# veh= vehicle('ford')
# print('it use vehicle init',veh.name,veh.stock)

# cr = car('BMW')
# cr.vehicle_stock()
# print('it use first car class then cehicle stock then vehicle ',cr.name,cr.stock)



#override




# class dog:
#     def __init__(self,name ,color):
#         self.name = name
#         self.color = color
#     def get_color(self):
        
#         return self.color
# class labrador(dog):
#     def __init__(self, name, color,fur = "double-coated"):
#         super().__init__(name, color)
#         self.fur = fur

# dg = dog('tom','black')
# lab_obj = labrador('mike','brown')
# print('it print dog class value',dg.name , dg.color)
# print('it print labrador class value',lab_obj.name,lab_obj.color)

# print('it run from labrador class to dog class which take value of labrador',lab_obj.get_color())
# print('it run directly parent class that is dog',dg.get_color())



# class vehicle:
#     def __init__(self,name ,vehicle= 'empty',number_of_wheel='None'):
#         self.name = name
#         self.vehicle = vehicle
#         self.number_of_wheel = number_of_wheel
#     def get_number_of_wheels(self):
        
#         return self.number_of_wheel
# class car(vehicle):
#     def whel_of_car(self):
#         self.vehicle= 'car'
#         self.number_of_wheel='4'
# class bike(vehicle):
#     def whel_of_bike(self):
#         self.vehicle = 'bike'
#         self.number_of_wheel = '2'
        
        
    

# parent_class_obj = vehicle('ns 200')
# print('it print from parent',parent_class_obj.name,parent_class_obj.vehicle,parent_class_obj.number_of_wheel)

# child_cls_obj_car = car(name='FOrd',)
# child_cls_obj_car.whel_of_car()
# print('it print form child class that is car ',child_cls_obj_car.name,child_cls_obj_car.vehicle,child_cls_obj_car.number_of_wheel)

# child_cls_obj_bike = bike(name= 'Pulsar')
# child_cls_obj_bike.whel_of_bike()
# print('it print form child class that is car ',child_cls_obj_bike.name,child_cls_obj_bike.vehicle,child_cls_obj_bike.number_of_wheel)

# Multilevel inheritance
# from mimetypes import init


# class First:
#     def __init__(self):
#         print('first')
        
    
# class Second:
#     def __init__(self):
#         print('second')
    
# class Third (First, Second):
#     def __init__(self):
#         print('third')
    

# Third()
# Third.test()
# it run from third to first to second to object

# Now with super

# class First():
#     def __init__(self):
#         super(First,self).__init__()
#         print('first')
        
    
# class Second():
#     def __init__(self):
        
#         print('second')
#     def test():
#         print('second test')
# class Third (First, Second):
#     def __init__(self):
#         super(Third,self).__init__()

#         print('third')
    
# Third()
### Mulilevel inheritance

# class First:
#     def __init__(self):
#         print('this is first inii')

#     def test():
#         print('first test')
# class Second(First):
#     def __init__(self):
#         super(Second,self).__init__()
#         print('this is second init')
#     def test():
#         print('second test')
# class Third(Second):
#     def __init__(self):
#         super(Third,self).__init__()
#         print ('third init')
#     # def test():
#     #     print('third test')


# Third()
# print(Third.mro())#method resoulution 
# Third.test()

# class parent:
#     def __init__(self):
#         print('parent')
# class father(parent):
#     def __init__(self):
#         print('father = Khem')
# class mother(father,parent):
#     def __init__(self):
#         #super(mother,self).__init__()
#         print('mothernmae = sunita')
    
        
        

# class child1(mother,father):
#     def __init__(self,age=28):
#         self.age=age
#         # super().__init__()
#         print('child1')
#     def get_age(self):
#         print(self.age)

# class child2(mother,father):
#     def __init__(self,age=12):
#         self.age = age
#         super(child2,self).__init__()
#         print('child2')
    
# class errors(mother,child1):
#     print('hello')

# Error



# print(child2.mro())
# print(child1.mro())
# print(father.mro())
# print(mother.mro())

# c2 = child2()
# c2.get_age()

# c1 = child1()
# c1.get_age()
