'''
1 ) Create a class Vehicle with attributes: color, brand, vehicle_type (car, bike, truck, etc.)
, number_of_wheels (make default=4), price. Then create its object, along with a method to access its brand
, vehicle type and price.
'''

# from cmath import sqrt
# from re import X


# class vehicle:
#     def __init__(self,color,brand,vehicle_type,price,number_of_wheel = 4):
#         self.color = color
#         self.brand= brand
#         self.vehicle_type = vehicle_type
#         self.price = price
#         self.number_of_wheels = number_of_wheel

#     def print_fun(self):
#         print(f'vehicle type {self.vehicle_type}')
#         print(f'My car brand is {self.brand} its color is {self.color}')
#         print(f'Price {self.price}')
#         print(f'{self.number_of_wheels} wheels car')
    
#     def __str__(self):
#          return print(f'king {self.price}')
        

# v = vehicle(color='red',brand='ford',vehicle_type='car',price=4000000)
        
# v.print_fun()


''' 2) 
Create a class Point where initial x,y values are initialized with 0. 
Create objects of class Point with x and y values passed to it. 

Eg: point p1 with x=2 and y=3 should be created.
		
Create a method to Calculate distance between two points. 

Formula: res = sqrt((x2-x1)^2 + (y2-y1)^2)   , where x1 and x2 are x values of p1 and p2 points resp.
 , y1 and y2 are y values of p1 and p2 resp.

Represent the point objects with x, y values in the format: (x,y). Eg: (2,3).

'''

# class point:
#     def __init__(self,x,y):
#         self.x = x
#         self.y =y
        

#     def method_sistance(self):
#         return sqrt((self.x2-self.x1)**2 +(self.y2-self.y1)**2)


        
        

# # a
# p1 =point(x=2,y=3)
# p2= point(x=6,y=6)
# print(p1.method_sistance())
##############According to teacher #############








''' question no. 3 
Create a class Employee, with attributes: name, emp_code, designation.
Generate 4 digit emp code (using static method)
Create a method to create an employee object and emp_code for it should be generated using the method: that you created in Q.3.a.
Create objects :
One directly by passing parameters to the class. 
Another by the method you created in Q.3.b.

 '''

#  ###############08/03/2022##############
# import random


# class employe:
#     def __init__(self,name,emp_code):
#         self.name = name
#         self.emp_code = emp_code
        

    
#     @staticmethod
#     def emp_code_generator():
#         emp_code =''.join([str (random.randint(0,9) )for i in range(4)])
        
#         return emp_code

#     @classmethod
#     def class_object(cls,name,emp_code=emp_code_generator()):
#         print(f'emp name is {name} and emp code is {emp_code}')
#         return cls(name,emp_code)

# emp = employe.class_object(name='sandip')
# print(emp, emp.emp_code)



'''
question no 4.
Create a TemperatureConverter class, where you need to convert between Kelvin , celsius and Fahrenheit . 
Eg: if degree is given in C, a method to convert C to K, and another to C to F.

Similarly, do for all three temperature measuring units. Hint: use static methods
'''

# class temprature:
    
#     def __init__(self,degree):
#         self.degree = degree
        
#     def pr(self):
#         print(self.degree)
#     @staticmethod
#     def far():
#         f = 32
#         return f
    
#     @staticmethod 
#     def kel():
#         k=273.15
#         return k
#     @classmethod
#     def c_k(cls, degree, kelvin = kel() ):
#         cls = degree +kelvin
#         return cls

#     @classmethod
#     def c_f(cls, degree, farn = far() ):
#         cls = (degree*1.8)+farn
#         return cls

        
# c_kel = temprature.c_k(32)
# print(c_kel)
# c_far = temprature.c_f(-40)
# print(c_far)

        

'''
Question no 5 

Create a parent class Vehicle with parameters brand, model and num of wheels. 
 Inherit it to Car and Bike, override the num of wheels attribute for car and
  bike respectively. 

'''


# class vehicle:
#     def __init__(self,name ,vehicle= 'empty',number_of_wheel='None'):
#         self.name = name
#         self.vehicle = vehicle
#         self.number_of_wheel = number_of_wheel
#     def get_number_of_wheels(self):
        
#         return self.number_of_wheel
# class car(vehicle):
#     def whel_of_car(self):
#         self.vehicle= 'car'
#         self.number_of_wheel='4'
# class bike(vehicle):
#     def whel_of_bike(self):
#         self.vehicle = 'bike'
#         self.number_of_wheel = '2'
        
        
    

# parent_class_obj = vehicle('ns 200')
# print('it print from parent',parent_class_obj.name,parent_class_obj.vehicle,parent_class_obj.number_of_wheel)

# child_cls_obj_car = car(name='FOrd',)
# child_cls_obj_car.whel_of_car()
# print('it print form child class that is car ',child_cls_obj_car.name,child_cls_obj_car.vehicle,child_cls_obj_car.number_of_wheel)

# child_cls_obj_bike = bike(name= 'Pulsar')
# child_cls_obj_bike.whel_of_bike()
# print('it print form child class that is car ',child_cls_obj_bike.name,child_cls_obj_bike.vehicle,child_cls_obj_bike.number_of_wheel)

# class vehicle:
#     def __init__(self,name ,vehicle= 'empty',number_of_wheel='None'):
#         self.name = name
#         self.vehicle = vehicle
#         self.number_of_wheel = number_of_wheel
#     def get_number_of_wheels(self):
        
#         return self.number_of_wheel
# class car(vehicle):
#     def __init__(self, name, vehicle='empty', number_of_wheel='None'):
#         super().__init__(name, vehicle, number_of_wheel)
#     def whel_of_car(self):
#         self.vehicle= 'car'
#         self.number_of_wheel='4'
# class bike(vehicle):
#     def whel_of_bike(self):
#         self.vehicle = 'bike'
#         self.number_of_wheel = '2'
        
        
    

# parent_class_obj = vehicle('ns 200')
# print('it print from parent',parent_class_obj.name,parent_class_obj.vehicle,parent_class_obj.number_of_wheel)

# child_cls_obj_car = car(name='FOrd',)
# child_cls_obj_car.whel_of_car()
# print('it print form child class that is car ',child_cls_obj_car.name,child_cls_obj_car.vehicle,child_cls_obj_car.number_of_wheel)

# child_cls_obj_bike = bike(name= 'Pulsar')
# child_cls_obj_bike.whel_of_bike()
# print('it print form child class that is car ',child_cls_obj_bike.name,child_cls_obj_bike.vehicle,child_cls_obj_bike.number_of_wheel)


'''
Questio no. 6 Create classes with inheritance as in the picture below. Add your own attributes to the classes.'''

# class parent:
#     def __init__(self):
#         print('parent')
# class father(parent):
#     def __init__(self):
#         print('father = Khem')
# class mother(father,parent):
#     def __init__(self):
#         #super(mother,self).__init__()
#         print('mothernmae = sunita')
    
        
        

# class child1(mother,father):
#     def __init__(self,age=28):
#         self.age=age
#         # super().__init__()
#         print('child1')
#     def get_age(self):
#         print(self.age)

# class child2(mother,father):
#     def __init__(self,age=12):
#         self.age = age
#         super(child2,self).__init__()
#         print('child2')
# multiple inheritance
# class grandpa:
#     def __init__(self):
#         print('grand pa')
# class father(grandpa):
#     def __init__(self):
#         super().__init__()
#         print('father')
# class son(father):
#     def __init__(self):
#         super(son,self).__init__()
#         print('son')
# # when super is added then it will call parent class inside of calling class

# son()
'''
Question no 7 
In order to make burger a chef needs at least the following ingredients:
• meat: pork, ham, chicken
• lettuce leaves
• tomato slices
sauce: bbq or mayonnaise
cheese

Get values of chicken meat, lettuce leaves and tomato slices from user. And create a normal sized burger. Use of OOP in Python.

'''


    